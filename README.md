# Ceramic kilns and workshops in the ancient world

This repository contains GeoJSON map files about ceramic kilns and workshops (_ateliers_) in the ancient world: Classical antiquity, Eastern civilizations, pre-Columbian Americas, etc.. if it is an archaeological feature, it will be good for this repository.

## How to contribute

You will need a GitHub account, and you will use <http://geojson.io/> (no registration needed) to create and edit map files.

If you work on an existing file, open it in GitHub and a map will be displayed on the screen. You can navigate the map and click on markers. Click the _Edit_ button in the upper right corner of the map. You will be presented with the JSON source for the map. Select all the JSON source and paste it in the editing frame of <http://geojson.io/>. You will see the same markers appear, but this map is editable: you can add more markers for other ceramic kilns. Don't worry too much about providing complete data: if you can, provide a `name` and a `source`.

If you start from scratch, go directly to <http://geojson.io/>, and use the “table” view to add data about each site:

* `name` can be the ancient placename, or more commonly the modern placename. Use the name that would be found in archaeological literature, so it's easier to find for others
* `source` is a stable URL that points to a book, a journal article, or any other published source of information about that kiln or atelier. You can use DOI URLs, Zotero URLs, Persée URLs, Wikipedia URLs, Pleiades URLs...

If any of the sites you're adding have an ID in another catalogue, please add it so it's easier to cross-check. For Classical antiquity, many sites (but not all!) will have a [Pleiades](http://pleiades.stoa.org) ID. Just add a `pleiades` column and fill in the ID (not the entire URL). Do the same for other catalogues.

When you are done editing the map in geojson.io, go to the “JSON” view, copy all the content and paste it in the GitHUb editing window, overwriting the previous content (your new map will contain also the previous content plus your contributions). Provide a commit message, a sort of comment, describing what you added (e.g. _sigillata workshops in the Guadalquivir valley_).

## What NOT to contribute

If there is agreement that there was a ceramic workshop at a certain site, but there is no archaeological evidence for the actual workshop, either don't add the site to the repository or add it in a separate file with clear indications. In general:

* either kiln remains or substantial discard heaps are good evidence
* some mis-fired pots in residential contexts are not good evidence
* petrographic or chemical evidence for production in a certain region is not good evidence (but could use a separate collection effort)
